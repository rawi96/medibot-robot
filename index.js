const mqtt = require("mqtt");

const express = require("express");
const app = express();

const robotTopic = "/medibot";
const appTopic = "/application";

let client;

let battery = 60;

const connectAndSubscribe = () => {
  const host = "localhost";
  const port = "1883";
  const clientId = "medibot-robot";
  const connectUrl = `mqtt://${host}:${port}`;

  client = mqtt.connect(connectUrl, {
    clientId,
    clean: true,
    connectTimeout: 4000,
    reconnectPeriod: 10000,
  });

  client.on("connect", () => {
    client.subscribe(robotTopic, (err) => {
      if (!err) {
        client.publish(appTopic, JSON.stringify({ patientTreatments: "?" }));
        console.log(JSON.stringify({ patientTreatments: "?" }));
      }
    });
  });
  return client;
};

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

const sendMqttMessageDelayed = async (message, update = {}) => {
  publishMessage = JSON.stringify({
    status: message,
    battery: --battery,
    update,
  });
  await delay(5000);
  client.publish(
    appTopic,
    publishMessage,
    { qos: 0, retain: false },
    (error) => {
      if (error) {
        console.error(error);
      }
      console.log(publishMessage);
    }
  );
};

const startPatientTreatment = async (json) => {
  if (!json.length) {
    console.log("No Patient Treatments planned");
    return;
  }

  await sendMqttMessageDelayed(
    `Starting Patient Treatments: ${json.map((p) => p.id + ", ")}`
  );

  await sendMqttMessageDelayed("Leaving Parking Place");

  await sendMqttMessageDelayed("Driving to Drug Store");

  await sendMqttMessageDelayed(
    `Taking ${json.map(
      (p) => p.treatment.drug.description + ", "
    )} from Drug Store`
  );

  if (json[0]) {
    await sendMqttMessageDelayed(
      `Driving to ${json[0].patient.room.description}`
    );
    await sendMqttMessageDelayed(
      `Treating Patient with ${json[0].treatment.drug.description}`
    );
    await sendMqttMessageDelayed(
      `Patient Treatment ${json[0].id} ended successfully`,
      {
        id: json[0].id,
        status: "successfully",
      }
    );
  }

  if (json[1]) {
    await sendMqttMessageDelayed(
      `Driving to ${json[1].patient.room.description}`
    );
    await sendMqttMessageDelayed(
      `Treating Patient with ${json[1].treatment.drug.description}`
    );
    await sendMqttMessageDelayed(
      `Patient Treatment ${json[1].id} ended failed`,
      {
        id: json[1].id,
        status: "failed",
      }
    );
  }

  if (json[2]) {
    await sendMqttMessageDelayed(
      `Driving to ${json[2].patient.room.description}`
    );
    await sendMqttMessageDelayed(
      `Treating Patient with ${json[2].treatment.drug.description}`
    );
    await sendMqttMessageDelayed(
      `Patient Treatment ${json[2].id} ended successfully`,
      {
        id: json[2].id,
        status: "successfully",
      }
    );
  }

  if (json[3]) {
    await sendMqttMessageDelayed(
      `Driving to ${json[3].patient.room.description}`
    );
    await sendMqttMessageDelayed(
      `Treating Patient with ${json[3].treatment.drug.description}`
    );
    await sendMqttMessageDelayed(
      `Patient Treatment ${json[3].id} ended successfully`,
      {
        id: json[3].id,
        status: "successfully",
      }
    );
  }

  await sendMqttMessageDelayed("Driving Back to Parking Place");
  await sendMqttMessageDelayed("Entering Parking Place");

  await sendMqttMessageDelayed(`Finishing Patient Treatments`);
  await sendMqttMessageDelayed("Waiting for new Patient Treatments");
};

app.listen(4001, () => {
  console.log(`MEDIBOT STARTED`);
  client = connectAndSubscribe();

  client.on("message", (topic, message) => {
    const decoded = decodeURIComponent(message);
    const json = JSON.parse(decoded);
    console.log(json);
    console.log(json[0].patient.room);
    console.log(json[0].treatment.drug);

    startPatientTreatment(json);
  });
});
